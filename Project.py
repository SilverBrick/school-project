def menu(): 
    print("1 : Book Registration")
    print("2 : Stats")
    print("3 : Book Data")
    print("4:  Book Issuing")
    print("5:  Book Returning")
    print("6:  Defaulter List")
    choice=int(input("Enter Your Choice:"))
    if choice==1:
        print("Book Registration Portal")
        register()
    elif choice==2:
        print("Library Statistic")
        print("Stats Is Loading...")
        stats()
    elif choice==3:
        show()
    elif choice==4:
          issue()
    elif choice==5:
          returning()
    elif choice==6:
          defaulter()
    else:
        exit()
def register():
    name=str(input("Enter Book Name:"))
    isbn=int(input("Enter ISBN Code:"))
    description=str(input("Enter Book Description"))
    category='other'
    print("Enter Category Of Book")
    print("1:English / 2:Mathematics / 3:Science / 4:Guides /5:General Studies  /6:Other")
    cat=int(input("Enter Your Category No:"))
    if cat==1:
        category='English'
    elif cat==2:
        category='Mathematics'
    elif cat==3:
        category='Science'
    elif cat==4:
        category='Guides'
    elif cat==5:
        category='General Studies'
    else:
        category='Other'
    import mysql.connector as mq
    conn=mq.connect(host='localhost',user='root',passwd='root',database='library',port=3306)
    cur=conn.cursor()
    query="insert into book values('{}',{},'{}','{}')".format(name,isbn,description,category)
    cur.execute(query)
    conn.commit()
    print("Data Feeded  Successfully")
    conn.close()
    menu()
def stats():
    import matplotlib.pyplot as p
    import numpy as np
    import mysql.connector as mq
    conn=mq.connect(host='localhost',user='root',passwd='root',database='library',port=3306)
    cur=conn.cursor()
    query="select count(*) from book where category='English';"
    cur.execute(query)
    res=cur.fetchall()
    res1=res[0]
    res2=str(res1)
    l=[]
    for i in res2:
        l.append(i)
    m=int(l[1])  
    x=[m,0,0,0]
    y=['Eng','Mathematics','Science','Other']
    p.bar(y,x,width=0.2)
    p.xlabel=("Book Category")
    p.ylabel=("No's Of Book")
    p.title("Library Book Count")
    p.show()
    conn.commit()
    conn.close()
    menu()
def issue():
    print("Book Issuing Portal")
    admno=int(input("Enter The Admisission Number :"))
    isbn=int(input("Enter The Isbn Number Of Book:"))
    import datetime
    import time
    ts = time.time()
    timestamp = datetime.datetime.fromtimestamp(ts).strftime('%d-%m-%Y')
    print("Issued On" ,timestamp)
    import mysql.connector as mq
    conn=mq.connect(host='localhost',user='root',passwd='root',database='library',port=3306)
    cur=conn.cursor()
    query="insert into issue values({},{},'{}');".format(admno,isbn,timestamp)
    cur.execute(query)
    print("Data Feeded!")
    conn.commit()
    conn.close()
    menu()
    #than feed this data to a table named issue which has common isbn row
def show():
    print("Book Details")
    from tabulate import tabulate
    import mysql.connector as mq
    conn=mq.connect(host='localhost',user='root',passwd='root',database='library',port=3306)
    cur=conn.cursor()
    query="select * from book;"
    cur.execute(query)
    res=cur.fetchall()
    res1 = tabulate(res, headers=['Name', 'ISBN-Code','Description','Category'], tablefmt='psql')
    print(res1)
    conn.commit()
    conn.close()
    menu()

def defaulter():
    print('Defaulter List')
    from tabulate import tabulate
    from datetime import datetime
    from datetime import date
    import mysql.connector as mq
    conn=mq.connect(host='localhost',user='root',passwd='root',database='library',port=3306)
    cur=conn.cursor()
    query="select admno, isbn, date from issue where date <= DATE_FORMAT(DATE_ADD(curdate(), INTERVAL -7 DAY),'%d-%m-%Y');"
    cur.execute(query)
    res=cur.fetchall()
    res1 = tabulate(res, headers=['Admno', 'ISBN-Code','Date Of Issue'], tablefmt='psql')
    print(res1)
    conn.commit()
    conn.close()
    menu()
    #those who have books for +6 days should be filtered out..
    
def returning():
    print("Book Returning Portal")
    a=int(input("Enter Your Admission Number:"))
    b=int(input("Enter Isbn No:"))
    from datetime import datetime
    from datetime import date
    import mysql.connector as mq
    conn=mq.connect(host='localhost',user='root',passwd='root',database='library',port=3306)
    cur=conn.cursor()
    query="select date from issue where admno={} and isbn={};".format(a,b)
    cur.execute(query)
    res=cur.fetchall()
    res1=str(res)
    L=[]
    for i in res1:
        L.append(i)
    sd=int(L[3]+L[4]) #submitting day
    sm=int(L[6]+L[7])
    sy=int(L[9]+L[10]+L[11]+L[12])
    now = datetime.now()
    t = now.strftime("%Y/%m/%d")
    t1=t.split("/")
    cy=int(t1[0])  #current year , #cm=current month #cd=current day
    cm=int(t1[1])
    cd=int(t1[2])
    f_date = date(sy, sm,sd)
    l_date = date(cy,cm,cd)
    delta = l_date - f_date
    diff=delta.days #difference btw issued date and submitting date
    fineday=diff-7 #no of days book have been kept after a week duration over!
    fine=fineday*5 #fine amount
    if diff >=8:
        print("You Need To Pay Rs",fine,"For Keeping The Book For",fineday,"Extra Days!!")
        print("If Fine Paid Enter 1 Or Else 2")
        fp=int(input("Enter Your Choice:"))
        if fp==1:
            querya="DELETE FROM issue WHERE isbn={}".format(b)
            cur.execute(querya)
            print("Book Is Returned Successfully !")
        else:
            print("Come Later With The Fine Amount !")
    elif diff <=8:
        queryb="DELETE FROM issue WHERE isbn={}".format(b)
        cur.execute(queryb)
        print("Submitted In",diff,"Days Only!")
    else:
        print("Error / Duplicacy Found !!")
    conn.commit()
    conn.close()
    menu() 


    
    
print("Welcome To Library Management CMS")

menu()
